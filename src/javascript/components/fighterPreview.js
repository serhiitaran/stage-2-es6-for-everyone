import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  if (fighter) {
    const fighterImage = createFighterImage(fighter);
    const { name, health, attack, defense } = fighter;
    const fighterInfo = createElement({
      tagName: 'div',
      className: 'fighter-preview__info'
    });

    fighterInfo.innerHTML = `
      <h3 class="fighter-preview__title"> ${name} </h3>
      <p class="fighter-preview__stat"> Health: ${health} </p>
      <p class="fighter-preview__stat"> Attack: ${attack} </p>
      <p class="fighter-preview__stat"> Defense: ${defense} </p>
    `;
    fighterElement.append(fighterImage, fighterInfo);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
