import { controls } from '../../constants/controls';

const CRITICAL_HIT_DELAY = 10000;

export async function fight(firstFighter, secondFighter) {
  const playerOne = createPlayer(firstFighter, 'left');
  const playerTwo = createPlayer(secondFighter, 'right');

  return new Promise((resolve) => {
    const activeKeys = new Set();
    document.addEventListener('keydown', fightHandler)
    document.addEventListener('keyup', (event) => {
      activeKeys.delete(event.code);
      disableBlock(playerOne, playerTwo, event.code);
      const isGameOver = checkGameOver(playerOne, playerTwo);
      if (isGameOver) {
        document.removeEventListener('keydown', fightHandler);
        const winner = playerOne.currentHealth <= 0 ? secondFighter : firstFighter;
        resolve(winner);
      }
    });
    function fightHandler(event) {
      activeKeys.add(event.code);
      fightAction(playerOne, playerTwo, activeKeys);
    }
  });
}

function createPlayer(fighter, side) {
  return {
    ...fighter,
    currentHealth: fighter.health,
    block: false,
    indicator: `#${side}-fighter-indicator`,
    canCriticalHit: true,
  };
}

function fightAction(playerOne, playerTwo, activeKeys) {
  switch (true) {
    case activeKeys.has(controls.PlayerOneAttack):
      attack(playerOne, playerTwo);
      break;
    case activeKeys.has(controls.PlayerTwoAttack):
      attack(playerTwo, playerOne);
      break;
    case checkCombination(controls.PlayerOneCriticalHitCombination, activeKeys):
      criticalHit(playerOne, playerTwo);
      break;
    case checkCombination(controls.PlayerTwoCriticalHitCombination, activeKeys):
      criticalHit(playerTwo, playerOne);
      break;
    case activeKeys.has(controls.PlayerOneBlock):
      playerOne.block = true;
      break;
    case activeKeys.has(controls.PlayerTwoBlock):
      playerTwo.block = true;
      break;
  }
}

function attack(attacker, defender) {
  if (!attacker.block && !defender.block) {
    defender.currentHealth -= getDamage(attacker, defender);
    changeHealthIndicator(defender);
  }
}

function criticalHit(attacker, defender) {
  const isCriticalHitAvailable = attacker.canCriticalHit && !attacker.block
  if (isCriticalHitAvailable) {
    defender.currentHealth -= getCriticalHitPower(attacker);
    changeHealthIndicator(defender);
    frezzeCriticalHit(attacker);
  }
}

function getCriticalHitPower({ attack }) {
  return attack * 2;
}

function frezzeCriticalHit(player) {
  player.canCriticalHit = false;
  setTimeout(() => player.canCriticalHit = true, CRITICAL_HIT_DELAY);
}

function checkCombination(combination, activeKeys) {
  for (let key of combination) {
    if (!activeKeys.has(key)) {
      return false;
    }
  }
  return true;
}

function disableBlock(playerOne, playerTwo, key) {
  switch (key) {
    case controls.PlayerOneBlock:
      playerOne.block = false;
      break;
    case controls.PlayerTwoBlock:
      playerTwo.block = false;
      break;
  }
}

function changeHealthIndicator(player) {
  const { health, currentHealth, indicator } = player;
  const lowHealth = 25;
  const lowHealthIndicatorColor = '#ba0303';
  const defaultIdicatorColor = '#ebd759';
  const playerIndicator = document.querySelector(indicator);
  const indicatorValue = (currentHealth / health) * 100;
  const indicatorColor = indicatorValue <= lowHealth ? lowHealthIndicatorColor : defaultIdicatorColor;
  playerIndicator.style.width =
    indicatorValue > 0 ? indicatorValue + '%' : 0;
  playerIndicator.style.backgroundColor = indicatorColor;
}

function checkGameOver(playerOne, playerTwo) {
  if (playerOne.currentHealth <= 0) {
    return playerTwo;
  } else if (playerTwo.currentHealth <= 0) {
    return playerOne;
  } else {
    return false;
  }
}


export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower({ attack }) {
  const criticalHitChance = getRandomNum(1, 2);
  return attack * criticalHitChance;
}

export function getBlockPower({ defense }) {
  const dodgeChance = getRandomNum(1, 2);
  return defense * dodgeChance;
}

function getRandomNum(min, max) {
  return Math.random() * (max - min) + min;
}
