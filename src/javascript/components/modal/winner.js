import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  const title = `${fighter.name} is win!`;
  const bodyElement = createFighterImage(fighter);
  showModal({
    title,
    bodyElement,
    onClose() {
      window.location.reload()
    }
  });
}
